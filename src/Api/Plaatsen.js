const Plaatsen = [
  {
    Gemeente: 'Aalst',
    StadOfGemeente: 'stad',
    Provincie: 'Oost-Vlaanderen',
    Oppervlakte: '78,12',
    AantalInwoners2016: 84.329,
    Deelgemeenten: 'Aalst, Gijzegem, Hofstade, Baardegem, Herdersem, Meldert, Moorsel, Erembodegem, Nieuwerkerken',

  },
  {
    Gemeente: 'Aalter[1]',
    StadOfGemeente: 'gem.',
    Provincie: 'Oost-Vlaanderen',
    Oppervlakte: '119,19',
    AantalInwoners2016: 28.435,
    Deelgemeenten: 'Aalter, Bellem, Knesselare, Lotenhulle, Poeke, Ursel',

  },
  {
    Gemeente: 'Aarschot',
    StadOfGemeente: 'stad',
    Provincie: 'Vlaams-Brabant',
    Oppervlakte: '62,52',
    AantalInwoners2016: 29.529,
    Deelgemeenten: 'Aarschot, Gelrode, Langdorp, Rillaar',

  },
  {
    Gemeente: 'Aartselaar',
    StadOfGemeente: 'gem.',
    Provincie: 'Antwerpen',
    Oppervlakte: '10,93',
    AantalInwoners2016: 14.262,
    Deelgemeenten: 'Aartselaar',

  },
  {
    Gemeente: 'Affligem',
    StadOfGemeente: 'gem.',
    Provincie: 'Vlaams-Brabant',
    Oppervlakte: '17,70',
    AantalInwoners2016: 13.079,
    Deelgemeenten: 'Essene, Hekelgem, Teralfene',

  },
  {
    Gemeente: 'Alken',
    StadOfGemeente: 'gem.',
    Provincie: 'Limburg',
    Oppervlakte: '28,14',
    AantalInwoners2016: 11.456,
    Deelgemeenten: 'Alken',

  },
  {
    Gemeente: 'Alveringem',
    StadOfGemeente: 'gem.',
    Provincie: 'West-Vlaanderen',
    Oppervlakte: '80,01',
    AantalInwoners2016: 5.039,
    Deelgemeenten: 'Alveringem, Hoogstade, Oeren, Sint-Rijkers, Beveren, Gijverinkhove, Izenberge, Leisele, Stavele',

  },
  {
    Gemeente: 'Antwerpen',
    StadOfGemeente: 'stad',
    Provincie: 'Antwerpen',
    Oppervlakte: '204,51',
    AantalInwoners2016: 517.042,
    Deelgemeenten: 'Antwerpen, Berendrecht-Zandvliet-Lillo, Deurne, Borgerhout, Merksem, Ekeren, Berchem, Hoboken, Wilrijk',

  },
  {
    Gemeente: 'Anzegem',
    StadOfGemeente: 'gem.',
    Provincie: 'West-Vlaanderen',
    Oppervlakte: '41,79',
    AantalInwoners2016: 14.589,
    Deelgemeenten: 'Anzegem, Gijzelbrechtegem, Ingooigem, Vichte, Kaster, Tiegem',

  },
  {
    Gemeente: 'Ardooie',
    StadOfGemeente: 'gem.',
    Provincie: 'West-Vlaanderen',
    Oppervlakte: '34,58',
    AantalInwoners2016: 9.053,
    Deelgemeenten: 'Ardooie, Koolskamp',

  },
  {
    Gemeente: 'Arendonk',
    StadOfGemeente: 'gem.',
    Provincie: 'Antwerpen',
    Oppervlakte: '55,38',
    AantalInwoners2016: 13.142,
    Deelgemeenten: 'Arendonk',

  },
  {
    Gemeente: 'As',
    StadOfGemeente: 'gem.',
    Provincie: 'Limburg',
    Oppervlakte: '22,07',
    AantalInwoners2016: 8.164,
    Deelgemeenten: 'As, Niel-bij-As',

  },
  {
    Gemeente: 'Asse',
    StadOfGemeente: 'gem.',
    Provincie: 'Vlaams-Brabant',
    Oppervlakte: '49,64',
    AantalInwoners2016: 32.402,
    Deelgemeenten: 'Asse, Bekkerzeel, Kobbegem, Mollem, Relegem, Zellik',

  },
  {
    Gemeente: 'Assenede',
    StadOfGemeente: 'gem.',
    Provincie: 'Oost-Vlaanderen',
    Oppervlakte: '87,22',
    AantalInwoners2016: 14.083,
    Deelgemeenten: 'Assenede, Boekhoute, Bassevelde, Oosteeklo',

  },
  {
    Gemeente: 'Avelgem',
    StadOfGemeente: 'gem.',
    Provincie: 'West-Vlaanderen',
    Oppervlakte: '21,75',
    AantalInwoners2016: 9.956,
    Deelgemeenten: 'Avelgem, Kerkhove, Waarmaarde, Outrijve, Bossuit',

  },
  {
    Gemeente: 'Baarle-Hertog',
    StadOfGemeente: 'gem.',
    Provincie: 'Antwerpen',
    Oppervlakte: '7,48',
    AantalInwoners2016: 2.663,
    Deelgemeenten: 'Baarle-Hertog',

  },
  {
    Gemeente: 'Balen',
    StadOfGemeente: 'gem.',
    Provincie: 'Antwerpen',
    Oppervlakte: '72,88',
    AantalInwoners2016: 22.168,
    Deelgemeenten: 'Balen, Olmen',

  },
  {
    Gemeente: 'Beernem',
    StadOfGemeente: 'gem.',
    Provincie: 'West-Vlaanderen',
    Oppervlakte: '71,68',
    AantalInwoners2016: 15.502,
    Deelgemeenten: 'Beernem, Oedelem, Sint-Joris',

  },
  {
    Gemeente: 'Beerse',
    StadOfGemeente: 'gem.',
    Provincie: 'Antwerpen',
    Oppervlakte: '37,48',
    AantalInwoners2016: 17.829,
    Deelgemeenten: 'Beerse, Vlimmeren',

  },
  {
    Gemeente: 'Beersel',
    StadOfGemeente: 'gem.',
    Provincie: 'Vlaams-Brabant',
    Oppervlakte: '30,01',
    AantalInwoners2016: 24.745,
    Deelgemeenten: 'Beersel, Lot, Alsemberg, Dworp, Huizingen',

  },
  {
    Gemeente: 'Begijnendijk',
    StadOfGemeente: 'gem.',
    Provincie: 'Vlaams-Brabant',
    Oppervlakte: '17,62',
    AantalInwoners2016: 10.058,
    Deelgemeenten: 'Begijnendijk, Betekom',

  },
  {
    Gemeente: 'Bekkevoort',
    StadOfGemeente: 'gem.',
    Provincie: 'Vlaams-Brabant',
    Oppervlakte: '37,17',
    AantalInwoners2016: 6.117,
    Deelgemeenten: 'Bekkevoort, Assent, Molenbeek-Wersbeek',

  },
  {
    Gemeente: 'Beringen',
    StadOfGemeente: 'stad',
    Provincie: 'Limburg',
    Oppervlakte: '78,30',
    AantalInwoners2016: 45.242,
    Deelgemeenten: 'Beringen, Beverlo, Koersel, Stal, Paal',

  },
  {
    Gemeente: 'Berlaar',
    StadOfGemeente: 'gem.',
    Provincie: 'Antwerpen',
    Oppervlakte: '24,57',
    AantalInwoners2016: 11.203,
    Deelgemeenten: 'Berlaar, Gestel',

  },
  {
    Gemeente: 'Berlare',
    StadOfGemeente: 'gem.',
    Provincie: 'Oost-Vlaanderen',
    Oppervlakte: '37,82',
    AantalInwoners2016: 14.758,
    Deelgemeenten: 'Berlare, Overmere, Uitbergen',

  },
  {
    Gemeente: 'Bertem',
    StadOfGemeente: 'gem.',
    Provincie: 'Vlaams-Brabant',
    Oppervlakte: '29,75',
    AantalInwoners2016: 9.803,
    Deelgemeenten: 'Bertem, Korbeek-Dijle, Leefdaal',

  },
  {
    Gemeente: 'Bever',
    StadOfGemeente: 'gem.[2]',
    Provincie: 'Vlaams-Brabant',
    Oppervlakte: '19,78',
    AantalInwoners2016: 2.151,
    Deelgemeenten: 'Bever',

  },
  {
    Gemeente: 'Beveren',
    StadOfGemeente: 'gem.',
    Provincie: 'Oost-Vlaanderen',
    Oppervlakte: '150,18',
    AantalInwoners2016: 47.573,
    Deelgemeenten: 'Beveren, Haasdonk, Melsele, Vrasene, Kallo, Doel, Kieldrecht, Verrebroek',

  },
  {
    Gemeente: 'Bierbeek',
    StadOfGemeente: 'gem.',
    Provincie: 'Vlaams-Brabant',
    Oppervlakte: '39,73',
    AantalInwoners2016: 9.953,
    Deelgemeenten: 'Bierbeek, Korbeek-Lo, Lovenjoel, Opvelp',

  },
  {
    Gemeente: 'Bilzen',
    StadOfGemeente: 'stad',
    Provincie: 'Limburg',
    Oppervlakte: '75,90',
    AantalInwoners2016: 31.829,
    Deelgemeenten: 'Bilzen, Beverst, Eigenbilzen, Grote-Spouwen, Hees, Kleine-Spouwen, Mopertingen, Munsterbilzen, Rijkhoven, Rosmeer, Waltwilder, Martenslinde, Hoelbeek',

  },
  {
    Gemeente: 'Blankenberge',
    StadOfGemeente: 'stad',
    Provincie: 'West-Vlaanderen',
    Oppervlakte: '17,41',
    AantalInwoners2016: 20.013,
    Deelgemeenten: 'Blankenberge, Uitkerke',

  },
  {
    Gemeente: 'Bocholt',
    StadOfGemeente: 'gem.',
    Provincie: 'Limburg',
    Oppervlakte: '59,34',
    AantalInwoners2016: 12.917,
    Deelgemeenten: 'Bocholt, Kaulille, Reppel',

  },
  {
    Gemeente: 'Boechout',
    StadOfGemeente: 'gem.',
    Provincie: 'Antwerpen',
    Oppervlakte: '20,66',
    AantalInwoners2016: 12.844,
    Deelgemeenten: 'Boechout, Vremde',

  },
  {
    Gemeente: 'Bonheiden',
    StadOfGemeente: 'gem.',
    Provincie: 'Antwerpen',
    Oppervlakte: '29,27',
    AantalInwoners2016: 14.704,
    Deelgemeenten: 'Bonheiden, Rijmenam',

  },
  {
    Gemeente: 'Boom',
    StadOfGemeente: 'gem.',
    Provincie: 'Antwerpen',
    Oppervlakte: '7,37',
    AantalInwoners2016: 17.737,
    Deelgemeenten: 'Boom',

  },
  {
    Gemeente: 'Boortmeerbeek',
    StadOfGemeente: 'gem.',
    Provincie: 'Vlaams-Brabant',
    Oppervlakte: '18,64',
    AantalInwoners2016: 12.102,
    Deelgemeenten: 'Boortmeerbeek, Hever',

  },
  {
    Gemeente: 'Borgloon',
    StadOfGemeente: 'stad',
    Provincie: 'Limburg',
    Oppervlakte: '51,12',
    AantalInwoners2016: 10.579,
    Deelgemeenten: 'Borgloon, Bommershoven, Broekom, Gors-Opleeuw, Gotem, Groot-Loon, Hendrieken, Hoepertingen, Jesseren, Kerniel, Kuttekoven, Rijkel, Voort',

  },
  {
    Gemeente: 'Bornem',
    StadOfGemeente: 'gem.',
    Provincie: 'Antwerpen',
    Oppervlakte: '45,76',
    AantalInwoners2016: 21.039,
    Deelgemeenten: 'Bornem, Weert, Mariekerke, Hingene',

  },
  {
    Gemeente: 'Borsbeek',
    StadOfGemeente: 'gem.',
    Provincie: 'Antwerpen',
    Oppervlakte: '3,92',
    AantalInwoners2016: 10.54,
    Deelgemeenten: 'Borsbeek',

  },
  {
    Gemeente: 'Boutersem',
    StadOfGemeente: 'gem.',
    Provincie: 'Vlaams-Brabant',
    Oppervlakte: '30,75',
    AantalInwoners2016: 8.132,
    Deelgemeenten: 'Boutersem, Kerkom, Neervelp, Roosbeek, Vertrijk, Willebringen',

  },
  {
    Gemeente: 'Brakel',
    StadOfGemeente: 'gem.',
    Provincie: 'Oost-Vlaanderen',
    Oppervlakte: '56,46',
    AantalInwoners2016: 14.787,
    Deelgemeenten: 'Elst, Everbeek, Michelbeke, Nederbrakel, Opbrakel, Zegelsem, Parike',

  },
  {
    Gemeente: 'Brasschaat',
    StadOfGemeente: 'gem.',
    Provincie: 'Antwerpen',
    Oppervlakte: '38,49',
    AantalInwoners2016: 37.673,
    Deelgemeenten: 'Brasschaat',

  },
  {
    Gemeente: 'Brecht',
    StadOfGemeente: 'gem.',
    Provincie: 'Antwerpen',
    Oppervlakte: '90,84',
    AantalInwoners2016: 28.534,
    Deelgemeenten: 'Brecht, Sint-Job-in-\'t-Goor, Sint-Lenaarts',

  },
  {
    Gemeente: 'Bredene',
    StadOfGemeente: 'gem.',
    Provincie: 'West-Vlaanderen',
    Oppervlakte: '13,08',
    AantalInwoners2016: 17.36,
    Deelgemeenten: 'Bredene',

  },
  {
    Gemeente: 'Bree',
    StadOfGemeente: 'stad',
    Provincie: 'Limburg',
    Oppervlakte: '64,96',
    AantalInwoners2016: 15.785,
    Deelgemeenten: 'Beek, Bree, Gerdingen, Opitter, Tongerlo',

  },
  {
    Gemeente: 'Brugge',
    StadOfGemeente: 'stad',
    Provincie: 'West-Vlaanderen',
    Oppervlakte: '138,40',
    AantalInwoners2016: 118.053,
    Deelgemeenten: 'Brugge, Koolkerke, Sint-Andries, Sint-Michiels, Assebroek, Sint-Kruis, Dudzele, Lissewege',

  },
  {
    Gemeente: 'Buggenhout',
    StadOfGemeente: 'gem.',
    Provincie: 'Oost-Vlaanderen',
    Oppervlakte: '25,25',
    AantalInwoners2016: 14.469,
    Deelgemeenten: 'Buggenhout, Opdorp',

  },
  {
    Gemeente: 'Damme',
    StadOfGemeente: 'stad',
    Provincie: 'West-Vlaanderen',
    Oppervlakte: '89,52',
    AantalInwoners2016: 10.907,
    Deelgemeenten: 'Damme, Hoeke, Lapscheure, Moerkerke, Oostkerke, Sijsele',

  },
  {
    Gemeente: 'De Haan',
    StadOfGemeente: 'gem.',
    Provincie: 'West-Vlaanderen',
    Oppervlakte: '42,17',
    AantalInwoners2016: 12.622,
    Deelgemeenten: 'Klemskerke, Wenduine, Vlissegem',

  },
  {
    Gemeente: 'De Panne',
    StadOfGemeente: 'gem.',
    Provincie: 'West-Vlaanderen',
    Oppervlakte: '23,90',
    AantalInwoners2016: 10.811,
    Deelgemeenten: 'De Panne, Adinkerke',

  },
  {
    Gemeente: 'De Pinte',
    StadOfGemeente: 'gem.',
    Provincie: 'Oost-Vlaanderen',
    Oppervlakte: '17,98',
    AantalInwoners2016: 10.367,
    Deelgemeenten: 'De Pinte, Zevergem',

  },
  {
    Gemeente: 'Deerlijk',
    StadOfGemeente: 'gem.',
    Provincie: 'West-Vlaanderen',
    Oppervlakte: '16,82',
    AantalInwoners2016: 11.738,
    Deelgemeenten: 'Deerlijk',

  },
  {
    Gemeente: 'Deinze[1]',
    StadOfGemeente: 'stad',
    Provincie: 'Oost-Vlaanderen',
    Oppervlakte: '127,53',
    AantalInwoners2016: 42.738,
    Deelgemeenten: 'Deinze, Astene, Bachte-Maria-Leerne, Gottem, Grammene, Hansbeke, Landegem, Meigem, Merendree, Nevele, Petegem-aan-de-Leie, Poesele, Sint-Martens-Leerne, Vinkt, Vosselare, Wontergem, Zeveren',

  },
  {
    Gemeente: 'Denderleeuw',
    StadOfGemeente: 'gem.',
    Provincie: 'Oost-Vlaanderen',
    Oppervlakte: '13,77',
    AantalInwoners2016: 19.688,
    Deelgemeenten: 'Denderleeuw, Iddergem, Welle',

  },
  {
    Gemeente: 'Dendermonde',
    StadOfGemeente: 'stad',
    Provincie: 'Oost-Vlaanderen',
    Oppervlakte: '55,67',
    AantalInwoners2016: 45.367,
    Deelgemeenten: 'Dendermonde, Appels, Baasrode, Grembergen, Mespelare, Oudegem, Schoonaarde, Sint-Gillis-bij-Dendermonde',

  },
  {
    Gemeente: 'Dentergem',
    StadOfGemeente: 'gem.',
    Provincie: 'West-Vlaanderen',
    Oppervlakte: '25,94',
    AantalInwoners2016: 8.374,
    Deelgemeenten: 'Dentergem, Markegem, Oeselgem, Wakken',

  },
  {
    Gemeente: 'Dessel',
    StadOfGemeente: 'gem.',
    Provincie: 'Antwerpen',
    Oppervlakte: '27,03',
    AantalInwoners2016: 9.42,
    Deelgemeenten: 'Dessel',

  },
  {
    Gemeente: 'Destelbergen',
    StadOfGemeente: 'gem.',
    Provincie: 'Oost-Vlaanderen',
    Oppervlakte: '26,56',
    AantalInwoners2016: 17.867,
    Deelgemeenten: 'Destelbergen, Heusden',

  },
  {
    Gemeente: 'Diepenbeek',
    StadOfGemeente: 'gem.',
    Provincie: 'Limburg',
    Oppervlakte: '41,19',
    AantalInwoners2016: 18.906,
    Deelgemeenten: 'Diepenbeek',

  },
  {
    Gemeente: 'Diest',
    StadOfGemeente: 'stad',
    Provincie: 'Vlaams-Brabant',
    Oppervlakte: '58,20',
    AantalInwoners2016: 23.538,
    Deelgemeenten: 'Diest, Deurne, Kaggevinne, Molenstede, Schaffen, Webbekom',

  },
  {
    Gemeente: 'Diksmuide',
    StadOfGemeente: 'stad',
    Provincie: 'West-Vlaanderen',
    Oppervlakte: '149,40',
    AantalInwoners2016: 16.551,
    Deelgemeenten: 'Diksmuide, Beerst, Esen, Kaaskerke, Keiem, Lampernisse, Leke, Nieuwkapelle, Oostkerke, Oudekapelle, Pervijze, Sint-Jacobs-Kapelle, Stuivekenskerke, Vladslo, Woumen',

  },
  {
    Gemeente: 'Dilbeek',
    StadOfGemeente: 'gem.',
    Provincie: 'Vlaams-Brabant',
    Oppervlakte: '41,18',
    AantalInwoners2016: 41.45,
    Deelgemeenten: 'Dilbeek, Sint-Martens-Bodegem, Sint-Ulriks-Kapelle, Itterbeek, Groot-Bijgaarden, Schepdaal',

  },
  {
    Gemeente: 'Dilsen-Stokkem',
    StadOfGemeente: 'stad',
    Provincie: 'Limburg',
    Oppervlakte: '65,61',
    AantalInwoners2016: 20.287,
    Deelgemeenten: 'Dilsen, Elen, Lanklaar, Rotem, Stokkem',

  },
  {
    Gemeente: 'Drogenbos',
    StadOfGemeente: 'gem.[2]',
    Provincie: 'Vlaams-Brabant',
    Oppervlakte: '2,49',
    AantalInwoners2016: 5.372,
    Deelgemeenten: 'Drogenbos',

  },
  {
    Gemeente: 'Duffel',
    StadOfGemeente: 'gem.',
    Provincie: 'Antwerpen',
    Oppervlakte: '22,71',
    AantalInwoners2016: 17.292,
    Deelgemeenten: 'Duffel',

  },
  {
    Gemeente: 'Edegem',
    StadOfGemeente: 'gem.',
    Provincie: 'Antwerpen',
    Oppervlakte: '8,65',
    AantalInwoners2016: 21.578,
    Deelgemeenten: 'Edegem',

  },
  {
    Gemeente: 'Eeklo',
    StadOfGemeente: 'stad',
    Provincie: 'Oost-Vlaanderen',
    Oppervlakte: '30,05',
    AantalInwoners2016: 20.561,
    Deelgemeenten: 'Eeklo',

  },
  {
    Gemeente: 'Erpe-Mere',
    StadOfGemeente: 'gem.',
    Provincie: 'Oost-Vlaanderen',
    Oppervlakte: '34,03',
    AantalInwoners2016: 19.72,
    Deelgemeenten: 'Aaigem, Bambrugge, Burst, Erondegem, Erpe, Mere, Ottergem, Vlekkem',

  },
  {
    Gemeente: 'Essen',
    StadOfGemeente: 'gem.',
    Provincie: 'Antwerpen',
    Oppervlakte: '47,48',
    AantalInwoners2016: 18.615,
    Deelgemeenten: 'Essen',

  },
  {
    Gemeente: 'Evergem',
    StadOfGemeente: 'gem.',
    Provincie: 'Oost-Vlaanderen',
    Oppervlakte: '75,04',
    AantalInwoners2016: 34.692,
    Deelgemeenten: 'Evergem, Ertvelde, Sleidinge, Kluizen',

  },
  {
    Gemeente: 'Galmaarden',
    StadOfGemeente: 'gem.',
    Provincie: 'Vlaams-Brabant',
    Oppervlakte: '34,93',
    AantalInwoners2016: 8.67,
    Deelgemeenten: 'Galmaarden, Tollembeek, Vollezele',

  },
  {
    Gemeente: 'Gavere',
    StadOfGemeente: 'gem.',
    Provincie: 'Oost-Vlaanderen',
    Oppervlakte: '31,34',
    AantalInwoners2016: 12.68,
    Deelgemeenten: 'Gavere, Asper, Baaigem, Dikkelvenne, Semmerzake, Vurste',

  },
  {
    Gemeente: 'Geel',
    StadOfGemeente: 'stad',
    Provincie: 'Antwerpen',
    Oppervlakte: '109,85',
    AantalInwoners2016: 39.225,
    Deelgemeenten: 'Geel',

  },
  {
    Gemeente: 'Geetbets',
    StadOfGemeente: 'gem.',
    Provincie: 'Vlaams-Brabant',
    Oppervlakte: '35,17',
    AantalInwoners2016: 5.97,
    Deelgemeenten: 'Geetbets, Grazen, Rummen',

  },
  {
    Gemeente: 'Genk',
    StadOfGemeente: 'stad',
    Provincie: 'Limburg',
    Oppervlakte: '87,85',
    AantalInwoners2016: 65.691,
    Deelgemeenten: 'Genk',

  },
  {
    Gemeente: 'Gent',
    StadOfGemeente: 'stad',
    Provincie: 'Oost-Vlaanderen',
    Oppervlakte: '156,18',
    AantalInwoners2016: 257.029,
    Deelgemeenten: 'Gent, Mariakerke, Drongen, Wondelgem, Sint-Amandsberg, Oostakker, Desteldonk, Mendonk, Sint-Kruis-Winkel, Gentbrugge, Ledeberg, Afsnee, Sint-Denijs-Westrem, Zwijnaarde',

  },
  {
    Gemeente: 'Geraardsbergen',
    StadOfGemeente: 'stad',
    Provincie: 'Oost-Vlaanderen',
    Oppervlakte: '79,71',
    AantalInwoners2016: 33.136,
    Deelgemeenten: 'Geraardsbergen, Goeferdinge, Moerbeke, Nederboelare, Onkerzele, Ophasselt, Overboelare, Viane, Zarlardinge, Grimminge, Idegem, Nieuwenhove, Schendelbeke, Smeerebbe-Vloerzegem, Waarbeke, Zandbergen',

  },
  {
    Gemeente: 'Gingelom',
    StadOfGemeente: 'gem.',
    Provincie: 'Limburg',
    Oppervlakte: '56,49',
    AantalInwoners2016: 8.377,
    Deelgemeenten: 'Gingelom, Boekhout, Jeuk, Kortijs, Montenaken, Niel-bij-Sint-Truiden, Vorsen, Borlo, Buvingen, Mielen-boven-Aalst, Muizen',

  },
  {
    Gemeente: 'Gistel',
    StadOfGemeente: 'stad',
    Provincie: 'West-Vlaanderen',
    Oppervlakte: '42,25',
    AantalInwoners2016: 11.851,
    Deelgemeenten: 'Gistel, Moere, Snaaskerke, Zevekote',

  },
  {
    Gemeente: 'Glabbeek',
    StadOfGemeente: 'gem.',
    Provincie: 'Vlaams-Brabant',
    Oppervlakte: '26,78',
    AantalInwoners2016: 5.305,
    Deelgemeenten: 'Bunsbeek, Glabbeek-Zuurbemde, Kapellen, Attenrode',

  },
  {
    Gemeente: 'Gooik',
    StadOfGemeente: 'gem.',
    Provincie: 'Vlaams-Brabant',
    Oppervlakte: '39,70',
    AantalInwoners2016: 9.212,
    Deelgemeenten: 'Gooik, Kester, Leerbeek, Oetingen',

  },
  {
    Gemeente: 'Grimbergen',
    StadOfGemeente: 'gem.',
    Provincie: 'Vlaams-Brabant',
    Oppervlakte: '38,59',
    AantalInwoners2016: 36.742,
    Deelgemeenten: 'Grimbergen, Humbeek, Beigem, Strombeek-Bever',

  },
  {
    Gemeente: 'Grobbendonk',
    StadOfGemeente: 'gem.',
    Provincie: 'Antwerpen',
    Oppervlakte: '28,36',
    AantalInwoners2016: 11.204,
    Deelgemeenten: 'Grobbendonk, Bouwel',

  },
  {
    Gemeente: 'Haacht',
    StadOfGemeente: 'gem.',
    Provincie: 'Vlaams-Brabant',
    Oppervlakte: '30,57',
    AantalInwoners2016: 14.296,
    Deelgemeenten: 'Haacht, Tildonk, Wespelaar',

  },
  {
    Gemeente: 'Haaltert',
    StadOfGemeente: 'gem.',
    Provincie: 'Oost-Vlaanderen',
    Oppervlakte: '30,30',
    AantalInwoners2016: 18.117,
    Deelgemeenten: 'Haaltert, Denderhoutem, Heldergem, Kerksken',

  },
  {
    Gemeente: 'Halen',
    StadOfGemeente: 'stad',
    Provincie: 'Limburg',
    Oppervlakte: '36,29',
    AantalInwoners2016: 9.566,
    Deelgemeenten: 'Halen, Loksbergen, Zelem',

  },
  {
    Gemeente: 'Halle',
    StadOfGemeente: 'stad',
    Provincie: 'Vlaams-Brabant',
    Oppervlakte: '44,40',
    AantalInwoners2016: 38.289,
    Deelgemeenten: 'Halle, Buizingen, Lembeek',

  },
  {
    Gemeente: 'Ham',
    StadOfGemeente: 'gem.',
    Provincie: 'Limburg',
    Oppervlakte: '32,69',
    AantalInwoners2016: 10.618,
    Deelgemeenten: 'Kwaadmechelen, Oostham',

  },
  {
    Gemeente: 'Hamme',
    StadOfGemeente: 'gem.',
    Provincie: 'Oost-Vlaanderen',
    Oppervlakte: '40,21',
    AantalInwoners2016: 24.702,
    Deelgemeenten: 'Hamme, Moerzeke',

  },
  {
    Gemeente: 'Hamont-Achel',
    StadOfGemeente: 'stad',
    Provincie: 'Limburg',
    Oppervlakte: '43,66',
    AantalInwoners2016: 14.356,
    Deelgemeenten: 'Achel, Hamont',

  },
  {
    Gemeente: 'Harelbeke',
    StadOfGemeente: 'stad',
    Provincie: 'West-Vlaanderen',
    Oppervlakte: '29,14',
    AantalInwoners2016: 27.536,
    Deelgemeenten: 'Harelbeke, Bavikhove, Hulste',

  },
  {
    Gemeente: 'Hasselt',
    StadOfGemeente: 'stad',
    Provincie: 'Limburg',
    Oppervlakte: '102,24',
    AantalInwoners2016: 76.685,
    Deelgemeenten: 'Hasselt, Sint-Lambrechts-Herk, Wimmertingen, Kermt, Spalbeek, Kuringen, Stokrooie, Stevoort',

  },
  {
    Gemeente: 'Hechtel-Eksel',
    StadOfGemeente: 'gem.',
    Provincie: 'Limburg',
    Oppervlakte: '76,70',
    AantalInwoners2016: 12.294,
    Deelgemeenten: 'Hechtel, Eksel',

  },
  {
    Gemeente: 'Heers',
    StadOfGemeente: 'gem.',
    Provincie: 'Limburg',
    Oppervlakte: '53,07',
    AantalInwoners2016: 7.205,
    Deelgemeenten: 'Heers, Batsheers, Gutschoven, Heks, Horpmaal, Klein-Gelmen, Mechelen-Bovelingen, Mettekoven, Opheers, Rukkelingen-Loon, Vechmaal, Veulen',

  },
  {
    Gemeente: 'Heist-op-den-Berg',
    StadOfGemeente: 'gem.',
    Provincie: 'Antwerpen',
    Oppervlakte: '86,46',
    AantalInwoners2016: 42.11,
    Deelgemeenten: 'Heist-op-den-Berg, Hallaar, Booischot, Itegem, Wiekevorst, Schriek',

  },
  {
    Gemeente: 'Hemiksem',
    StadOfGemeente: 'gem.',
    Provincie: 'Antwerpen',
    Oppervlakte: '5,44',
    AantalInwoners2016: 11.148,
    Deelgemeenten: 'Hemiksem',

  },
  {
    Gemeente: 'Herent',
    StadOfGemeente: 'gem.',
    Provincie: 'Vlaams-Brabant',
    Oppervlakte: '32,73',
    AantalInwoners2016: 21.213,
    Deelgemeenten: 'Herent, Veltem-Beisem, Winksele',

  },
  {
    Gemeente: 'Herentals',
    StadOfGemeente: 'stad',
    Provincie: 'Antwerpen',
    Oppervlakte: '48,56',
    AantalInwoners2016: 27.728,
    Deelgemeenten: 'Herentals, Morkhoven, Noorderwijk',

  },
  {
    Gemeente: 'Herenthout',
    StadOfGemeente: 'gem.',
    Provincie: 'Antwerpen',
    Oppervlakte: '23,55',
    AantalInwoners2016: 8.806,
    Deelgemeenten: 'Herenthout',

  },
  {
    Gemeente: 'Herk-de-Stad',
    StadOfGemeente: 'stad',
    Provincie: 'Limburg',
    Oppervlakte: '42,83',
    AantalInwoners2016: 12.608,
    Deelgemeenten: 'Herk-de-Stad, Berbroek, Donk, Schulen',

  },
  {
    Gemeente: 'Herne',
    StadOfGemeente: 'gem.',
    Provincie: 'Vlaams-Brabant',
    Oppervlakte: '44,63',
    AantalInwoners2016: 6.617,
    Deelgemeenten: 'Herne, Herfelingen, Sint-Pieters-Kapelle',

  },
  {
    Gemeente: 'Herselt',
    StadOfGemeente: 'gem.',
    Provincie: 'Antwerpen',
    Oppervlakte: '52,32',
    AantalInwoners2016: 14.491,
    Deelgemeenten: 'Herselt, Ramsel',

  },
  {
    Gemeente: 'Herstappe',
    StadOfGemeente: 'gem.[2]',
    Provincie: 'Limburg',
    Oppervlakte: '1,35',
    AantalInwoners2016: 89,
    Deelgemeenten: 'Herstappe',

  },
  {
    Gemeente: 'Herzele',
    StadOfGemeente: 'gem.',
    Provincie: 'Oost-Vlaanderen',
    Oppervlakte: '47,40',
    AantalInwoners2016: 17.55,
    Deelgemeenten: 'Herzele, Hillegem, Sint-Antelinks, Sint-Lievens-Esse, Steenhuize-Wijnhuize, Woubrechtegem, Ressegem, Borsbeke',

  },
  {
    Gemeente: 'Heusden-Zolder',
    StadOfGemeente: 'gem.',
    Provincie: 'Limburg',
    Oppervlakte: '53,23',
    AantalInwoners2016: 33.011,
    Deelgemeenten: 'Heusden, Zolder',

  },
  {
    Gemeente: 'Heuvelland',
    StadOfGemeente: 'gem.',
    Provincie: 'West-Vlaanderen',
    Oppervlakte: '94,24',
    AantalInwoners2016: 7.827,
    Deelgemeenten: 'Nieuwkerke, Dranouter, Wulvergem, Wijtschate, Westouter, Kemmel, Loker',

  },
  {
    Gemeente: 'Hoegaarden',
    StadOfGemeente: 'gem.',
    Provincie: 'Vlaams-Brabant',
    Oppervlakte: '33,93',
    AantalInwoners2016: 6.891,
    Deelgemeenten: 'Hoegaarden, Meldert, Outgaarden',

  },
  {
    Gemeente: 'Hoeilaart',
    StadOfGemeente: 'gem.',
    Provincie: 'Vlaams-Brabant',
    Oppervlakte: '20,43',
    AantalInwoners2016: 10.915,
    Deelgemeenten: 'Hoeilaart',

  },
  {
    Gemeente: 'Hoeselt',
    StadOfGemeente: 'gem.',
    Provincie: 'Limburg',
    Oppervlakte: '30,02',
    AantalInwoners2016: 9.643,
    Deelgemeenten: 'Hoeselt, Romershoven, Sint-Huibrechts-Hern, Werm, Schalkhoven',

  },
  {
    Gemeente: 'Holsbeek',
    StadOfGemeente: 'gem.',
    Provincie: 'Vlaams-Brabant',
    Oppervlakte: '38,50',
    AantalInwoners2016: 9.935,
    Deelgemeenten: 'Holsbeek, Kortrijk-Dutsel, Sint-Pieters-Rode, Nieuwrode',

  },
  {
    Gemeente: 'Hooglede',
    StadOfGemeente: 'gem.',
    Provincie: 'West-Vlaanderen',
    Oppervlakte: '37,84',
    AantalInwoners2016: 10.016,
    Deelgemeenten: 'Hooglede, Gits',

  },
  {
    Gemeente: 'Hoogstraten',
    StadOfGemeente: 'stad',
    Provincie: 'Antwerpen',
    Oppervlakte: '105,32',
    AantalInwoners2016: 21.183,
    Deelgemeenten: 'Hoogstraten, Meer, Minderhout, Wortel, Meerle',

  },
  {
    Gemeente: 'Horebeke',
    StadOfGemeente: 'gem.',
    Provincie: 'Oost-Vlaanderen',
    Oppervlakte: '11,20',
    AantalInwoners2016: 2,
    Deelgemeenten: 'Sint-Kornelis-Horebeke, Sint-Maria-Horebeke',

  },
  {
    Gemeente: 'Houthalen-Helchteren',
    StadOfGemeente: 'gem.',
    Provincie: 'Limburg',
    Oppervlakte: '78,27',
    AantalInwoners2016: 30.592,
    Deelgemeenten: 'Helchteren, Houthalen',

  },
  {
    Gemeente: 'Houthulst',
    StadOfGemeente: 'gem.',
    Provincie: 'West-Vlaanderen',
    Oppervlakte: '55,89',
    AantalInwoners2016: 10.037,
    Deelgemeenten: 'Houthulst, Klerken, Merkem',

  },
  {
    Gemeente: 'Hove',
    StadOfGemeente: 'gem.',
    Provincie: 'Antwerpen',
    Oppervlakte: '5,99',
    AantalInwoners2016: 8.141,
    Deelgemeenten: 'Hove',

  },
  {
    Gemeente: 'Huldenberg',
    StadOfGemeente: 'gem.',
    Provincie: 'Vlaams-Brabant',
    Oppervlakte: '39,64',
    AantalInwoners2016: 9.743,
    Deelgemeenten: 'Huldenberg, Loonbeek, Neerijse, Ottenburg, Sint-Agatha-Rode',

  },
  {
    Gemeente: 'Hulshout',
    StadOfGemeente: 'gem.',
    Provincie: 'Antwerpen',
    Oppervlakte: '17,35',
    AantalInwoners2016: 10.255,
    Deelgemeenten: 'Hulshout, Houtvenne, Westmeerbeek',

  },
  {
    Gemeente: 'Ichtegem',
    StadOfGemeente: 'gem.',
    Provincie: 'West-Vlaanderen',
    Oppervlakte: '45,33',
    AantalInwoners2016: 13.945,
    Deelgemeenten: 'Ichtegem, Bekegem, Eernegem',

  },
  {
    Gemeente: 'Ieper',
    StadOfGemeente: 'stad',
    Provincie: 'West-Vlaanderen',
    Oppervlakte: '130,61',
    AantalInwoners2016: 34.959,
    Deelgemeenten: 'Ieper, Brielen, Dikkebus, Sint-Jan, Hollebeke, Voormezele, Zillebeke, Boezinge, Zuidschote, Elverdinge, Vlamertinge',

  },
  {
    Gemeente: 'Ingelmunster',
    StadOfGemeente: 'gem.',
    Provincie: 'West-Vlaanderen',
    Oppervlakte: '16,16',
    AantalInwoners2016: 10.792,
    Deelgemeenten: 'Ingelmunster',

  },
  {
    Gemeente: 'Izegem',
    StadOfGemeente: 'stad',
    Provincie: 'West-Vlaanderen',
    Oppervlakte: '25,48',
    AantalInwoners2016: 27.55,
    Deelgemeenten: 'Izegem, Emelgem, Kachtem',

  },
  {
    Gemeente: 'Jabbeke',
    StadOfGemeente: 'gem.',
    Provincie: 'West-Vlaanderen',
    Oppervlakte: '53,76',
    AantalInwoners2016: 13.862,
    Deelgemeenten: 'Jabbeke, Snellegem, Stalhille, Varsenare, Zerkegem',

  },
  {
    Gemeente: 'Kalmthout',
    StadOfGemeente: 'gem.',
    Provincie: 'Antwerpen',
    Oppervlakte: '59,45',
    AantalInwoners2016: 18.336,
    Deelgemeenten: 'Kalmthout',

  },
  {
    Gemeente: 'Kampenhout',
    StadOfGemeente: 'gem.',
    Provincie: 'Vlaams-Brabant',
    Oppervlakte: '33,49',
    AantalInwoners2016: 11.689,
    Deelgemeenten: 'Kampenhout, Berg, Buken, Nederokkerzeel',

  },
  {
    Gemeente: 'Kapellen',
    StadOfGemeente: 'gem.',
    Provincie: 'Antwerpen',
    Oppervlakte: '37,11',
    AantalInwoners2016: 26.745,
    Deelgemeenten: 'Kapellen',

  },
  {
    Gemeente: 'Kapelle-op-den-Bos',
    StadOfGemeente: 'gem.',
    Provincie: 'Vlaams-Brabant',
    Oppervlakte: '15,25',
    AantalInwoners2016: 9.37,
    Deelgemeenten: 'Kapelle-op-den-Bos, Nieuwenrode, Ramsdonk',

  },
  {
    Gemeente: 'Kaprijke',
    StadOfGemeente: 'gem.',
    Provincie: 'Oost-Vlaanderen',
    Oppervlakte: '33,71',
    AantalInwoners2016: 6.321,
    Deelgemeenten: 'Kaprijke, Lembeke',

  },
  {
    Gemeente: 'Kasterlee',
    StadOfGemeente: 'gem.',
    Provincie: 'Antwerpen',
    Oppervlakte: '71,52',
    AantalInwoners2016: 18.331,
    Deelgemeenten: 'Kasterlee, Lichtaart, Tielen',

  },
  {
    Gemeente: 'Keerbergen',
    StadOfGemeente: 'gem.',
    Provincie: 'Vlaams-Brabant',
    Oppervlakte: '18,39',
    AantalInwoners2016: 12.831,
    Deelgemeenten: 'Keerbergen',

  },
  {
    Gemeente: 'Kinrooi',
    StadOfGemeente: 'gem.',
    Provincie: 'Limburg',
    Oppervlakte: '54,76',
    AantalInwoners2016: 12.309,
    Deelgemeenten: 'Kinrooi, Kessenich, Molenbeersel, Ophoven',

  },
  {
    Gemeente: 'Kluisbergen',
    StadOfGemeente: 'gem.',
    Provincie: 'Oost-Vlaanderen',
    Oppervlakte: '30,38',
    AantalInwoners2016: 6.445,
    Deelgemeenten: 'Berchem, Kwaremont, Ruien, Zulzeke',

  },
  {
    Gemeente: 'Knokke-Heist',
    StadOfGemeente: 'gem.',
    Provincie: 'West-Vlaanderen',
    Oppervlakte: '56,44',
    AantalInwoners2016: 33.311,
    Deelgemeenten: 'Knokke, Westkapelle, Heist, Ramskapelle',

  },
  {
    Gemeente: 'Koekelare',
    StadOfGemeente: 'gem.',
    Provincie: 'West-Vlaanderen',
    Oppervlakte: '39,19',
    AantalInwoners2016: 8.75,
    Deelgemeenten: 'Koekelare, Bovekerke, Zande',

  },
  {
    Gemeente: 'Koksijde',
    StadOfGemeente: 'gem.',
    Provincie: 'West-Vlaanderen',
    Oppervlakte: '43,96',
    AantalInwoners2016: 22.074,
    Deelgemeenten: 'Koksijde, Oostduinkerke, Wulpen',

  },
  {
    Gemeente: 'Kontich',
    StadOfGemeente: 'gem.',
    Provincie: 'Antwerpen',
    Oppervlakte: '23,67',
    AantalInwoners2016: 20.873,
    Deelgemeenten: 'Kontich, Waarloos',

  },
  {
    Gemeente: 'Kortemark',
    StadOfGemeente: 'gem.',
    Provincie: 'West-Vlaanderen',
    Oppervlakte: '55,00',
    AantalInwoners2016: 12.415,
    Deelgemeenten: 'Kortemark, Handzame, Werken, Zarren',

  },
  {
    Gemeente: 'Kortenaken',
    StadOfGemeente: 'gem.',
    Provincie: 'Vlaams-Brabant',
    Oppervlakte: '49,10',
    AantalInwoners2016: 7.93,
    Deelgemeenten: 'Kortenaken, Ransberg, Hoeleden, Kersbeek-Miskom, Waanrode',

  },
  {
    Gemeente: 'Kortenberg',
    StadOfGemeente: 'gem.',
    Provincie: 'Vlaams-Brabant',
    Oppervlakte: '34,52',
    AantalInwoners2016: 19.714,
    Deelgemeenten: 'Kortenberg, Erps-Kwerps, Everberg, Meerbeek',

  },
  {
    Gemeente: 'Kortessem',
    StadOfGemeente: 'gem.',
    Provincie: 'Limburg',
    Oppervlakte: '33,90',
    AantalInwoners2016: 8.359,
    Deelgemeenten: 'Kortessem, Vliermaalroot, Wintershoven, Guigoven, Vliermaal',

  },
  {
    Gemeente: 'Kortrijk',
    StadOfGemeente: 'stad',
    Provincie: 'West-Vlaanderen',
    Oppervlakte: '80,02',
    AantalInwoners2016: 75.506,
    Deelgemeenten: 'Kortrijk, Bissegem, Heule, Bellegem, Kooigem, Marke, Rollegem, Aalbeke',

  },
  {
    Gemeente: 'Kraainem',
    StadOfGemeente: 'gem.[2]',
    Provincie: 'Vlaams-Brabant',
    Oppervlakte: '5,80',
    AantalInwoners2016: 13.713,
    Deelgemeenten: 'Kraainem',

  },
  {
    Gemeente: 'Kruibeke',
    StadOfGemeente: 'gem.',
    Provincie: 'Oost-Vlaanderen',
    Oppervlakte: '33,42',
    AantalInwoners2016: 16.541,
    Deelgemeenten: 'Kruibeke, Bazel, Rupelmonde',

  },
  {
    Gemeente: 'Kruisem[1]',
    StadOfGemeente: 'gem.',
    Provincie: 'Oost-Vlaanderen',
    Oppervlakte: '70,7',
    AantalInwoners2016: 15.664,
    Deelgemeenten: 'Huise, Kruishoutem, Nokere, Ouwegem, Wannegem-Lede, Zingem',

  },
  {
    Gemeente: 'Kuurne',
    StadOfGemeente: 'gem.',
    Provincie: 'West-Vlaanderen',
    Oppervlakte: '10,01',
    AantalInwoners2016: 13.14,
    Deelgemeenten: 'Kuurne',

  },
  {
    Gemeente: 'Laakdal',
    StadOfGemeente: 'gem.',
    Provincie: 'Antwerpen',
    Oppervlakte: '42,48',
    AantalInwoners2016: 15.851,
    Deelgemeenten: 'Eindhout, Vorst, Varendonk, Veerle',

  },
  {
    Gemeente: 'Laarne',
    StadOfGemeente: 'gem.',
    Provincie: 'Oost-Vlaanderen',
    Oppervlakte: '32,07',
    AantalInwoners2016: 12.515,
    Deelgemeenten: 'Laarne, Kalken',

  },
  {
    Gemeente: 'Lanaken',
    StadOfGemeente: 'gem.',
    Provincie: 'Limburg',
    Oppervlakte: '59,00',
    AantalInwoners2016: 25.793,
    Deelgemeenten: 'Lanaken, Gellik, Neerharen, Veldwezelt, Rekem',

  },
  {
    Gemeente: 'Landen',
    StadOfGemeente: 'stad',
    Provincie: 'Vlaams-Brabant',
    Oppervlakte: '54,05',
    AantalInwoners2016: 15.944,
    Deelgemeenten: 'Landen, Eliksem, Ezemaal, Laar, Neerwinden, Overwinden, Rumsdorp, Wange, Waasmont, Walsbets, Walshoutem, Wezeren, Attenhoven, Neerlanden',

  },
  {
    Gemeente: 'Langemark-Poelkapelle',
    StadOfGemeente: 'gem.',
    Provincie: 'West-Vlaanderen',
    Oppervlakte: '52,53',
    AantalInwoners2016: 7.94,
    Deelgemeenten: 'Bikschote, Langemark, Poelkapelle',

  },
  {
    Gemeente: 'Lebbeke',
    StadOfGemeente: 'gem.',
    Provincie: 'Oost-Vlaanderen',
    Oppervlakte: '26,92',
    AantalInwoners2016: 18.843,
    Deelgemeenten: 'Lebbeke, Denderbelle, Wieze',

  },
  {
    Gemeente: 'Lede',
    StadOfGemeente: 'gem.',
    Provincie: 'Oost-Vlaanderen',
    Oppervlakte: '29,69',
    AantalInwoners2016: 18.399,
    Deelgemeenten: 'Lede, Impe, Oordegem, Smetlede, Wanzele',

  },
  {
    Gemeente: 'Ledegem',
    StadOfGemeente: 'gem.',
    Provincie: 'West-Vlaanderen',
    Oppervlakte: '24,76',
    AantalInwoners2016: 9.53,
    Deelgemeenten: 'Ledegem, Rollegem-Kapelle, Sint-Eloois-Winkel',

  },
  {
    Gemeente: 'Lendelede',
    StadOfGemeente: 'gem.',
    Provincie: 'West-Vlaanderen',
    Oppervlakte: '13,15',
    AantalInwoners2016: 5.746,
    Deelgemeenten: 'Lendelede',

  },
  {
    Gemeente: 'Lennik',
    StadOfGemeente: 'gem.',
    Provincie: 'Vlaams-Brabant',
    Oppervlakte: '30,80',
    AantalInwoners2016: 9.033,
    Deelgemeenten: 'Gaasbeek, Sint-Kwintens-Lennik, Sint-Martens-Lennik',

  },
  {
    Gemeente: 'Leopoldsburg',
    StadOfGemeente: 'gem.',
    Provincie: 'Limburg',
    Oppervlakte: '22,49',
    AantalInwoners2016: 15.358,
    Deelgemeenten: 'Leopoldsburg, Heppen',

  },
  {
    Gemeente: 'Leuven',
    StadOfGemeente: 'stad',
    Provincie: 'Vlaams-Brabant',
    Oppervlakte: '56,63',
    AantalInwoners2016: 99.288,
    Deelgemeenten: 'Leuven, Heverlee, Kessel-Lo, Wilsele, Wijgmaal',

  },
  {
    Gemeente: 'Lichtervelde',
    StadOfGemeente: 'gem.',
    Provincie: 'West-Vlaanderen',
    Oppervlakte: '25,93',
    AantalInwoners2016: 8.688,
    Deelgemeenten: 'Lichtervelde',

  },
  {
    Gemeente: 'Liedekerke',
    StadOfGemeente: 'gem.',
    Provincie: 'Vlaams-Brabant',
    Oppervlakte: '10,08',
    AantalInwoners2016: 12.919,
    Deelgemeenten: 'Liedekerke',

  },
  {
    Gemeente: 'Lier',
    StadOfGemeente: 'stad',
    Provincie: 'Antwerpen',
    Oppervlakte: '49,70',
    AantalInwoners2016: 34.905,
    Deelgemeenten: 'Lier, Koningshooikt',

  },
  {
    Gemeente: 'Lierde',
    StadOfGemeente: 'gem.',
    Provincie: 'Oost-Vlaanderen',
    Oppervlakte: '26,13',
    AantalInwoners2016: 6.55,
    Deelgemeenten: 'Deftinge, Sint-Maria-Lierde, Hemelveerdegem, Sint-Martens-Lierde',

  },
  {
    Gemeente: 'Lievegem[1]',
    StadOfGemeente: 'gem.',
    Provincie: 'Oost-Vlaanderen',
    Oppervlakte: '80,17',
    AantalInwoners2016: 25.842,
    Deelgemeenten: 'Zomergem, Oostwinkel, Ronsele, Lovendegem, Vinderhoute, Waarschoot',

  },
  {
    Gemeente: 'Lille',
    StadOfGemeente: 'gem.',
    Provincie: 'Antwerpen',
    Oppervlakte: '59,40',
    AantalInwoners2016: 16.404,
    Deelgemeenten: 'Lille, Gierle, Poederlee, Wechelderzande',

  },
  {
    Gemeente: 'Linkebeek',
    StadOfGemeente: 'gem.[2]',
    Provincie: 'Vlaams-Brabant',
    Oppervlakte: '4,15',
    AantalInwoners2016: 4.752,
    Deelgemeenten: 'Linkebeek',

  },
  {
    Gemeente: 'Lint',
    StadOfGemeente: 'gem.',
    Provincie: 'Antwerpen',
    Oppervlakte: '5,57',
    AantalInwoners2016: 8.821,
    Deelgemeenten: 'Lint',

  },
  {
    Gemeente: 'Linter',
    StadOfGemeente: 'gem.',
    Provincie: 'Vlaams-Brabant',
    Oppervlakte: '36,37',
    AantalInwoners2016: 7.188,
    Deelgemeenten: 'Drieslinter, Melkwezer, Neerhespen, Neerlinter, Orsmaal-Gussenhoven, Overhespen, Wommersom',

  },
  {
    Gemeente: 'Lochristi',
    StadOfGemeente: 'gem.',
    Provincie: 'Oost-Vlaanderen',
    Oppervlakte: '60,34',
    AantalInwoners2016: 22.117,
    Deelgemeenten: 'Lochristi, Beervelde, Zaffelare, Zeveneken',

  },
  {
    Gemeente: 'Lokeren',
    StadOfGemeente: 'stad',
    Provincie: 'Oost-Vlaanderen',
    Oppervlakte: '67,50',
    AantalInwoners2016: 40.626,
    Deelgemeenten: 'Lokeren, Daknam, Eksaarde',

  },
  {
    Gemeente: 'Lommel',
    StadOfGemeente: 'stad',
    Provincie: 'Limburg',
    Oppervlakte: '102,37',
    AantalInwoners2016: 33.957,
    Deelgemeenten: 'Lommel',

  },
  {
    Gemeente: 'Londerzeel',
    StadOfGemeente: 'gem.',
    Provincie: 'Vlaams-Brabant',
    Oppervlakte: '36,29',
    AantalInwoners2016: 18.274,
    Deelgemeenten: 'Londerzeel, Malderen, Steenhuffel',

  },
  {
    Gemeente: 'Lo-Reninge',
    StadOfGemeente: 'stad',
    Provincie: 'West-Vlaanderen',
    Oppervlakte: '62,94',
    AantalInwoners2016: 3.277,
    Deelgemeenten: 'Lo, Noordschote, Pollinkhove, Reninge',

  },
  {
    Gemeente: 'Lubbeek',
    StadOfGemeente: 'gem.',
    Provincie: 'Vlaams-Brabant',
    Oppervlakte: '46,13',
    AantalInwoners2016: 14.206,
    Deelgemeenten: 'Lubbeek, Linden, Binkom, Pellenberg',

  },
  {
    Gemeente: 'Lummen',
    StadOfGemeente: 'gem.',
    Provincie: 'Limburg',
    Oppervlakte: '53,38',
    AantalInwoners2016: 14.603,
    Deelgemeenten: 'Lummen, Linkhout, Meldert',

  },
  {
    Gemeente: 'Maarkedal',
    StadOfGemeente: 'gem.',
    Provincie: 'Oost-Vlaanderen',
    Oppervlakte: '46,63',
    AantalInwoners2016: 6.281,
    Deelgemeenten: 'Etikhove, Maarke-Kerkem, Nukerke, Schorisse',

  },
  {
    Gemeente: 'Maaseik',
    StadOfGemeente: 'stad',
    Provincie: 'Limburg',
    Oppervlakte: '76,94',
    AantalInwoners2016: 25.133,
    Deelgemeenten: 'Maaseik, Neeroeteren, Opoeteren',

  },
  {
    Gemeente: 'Maasmechelen',
    StadOfGemeente: 'gem.',
    Provincie: 'Limburg',
    Oppervlakte: '76,28',
    AantalInwoners2016: 37.696,
    Deelgemeenten: 'Eisden, Leut, Mechelen-aan-de-Maas, Meeswijk, Opgrimbie, Vucht, Boorsem, Uikhoven',

  },
  {
    Gemeente: 'Machelen',
    StadOfGemeente: 'gem.',
    Provincie: 'Vlaams-Brabant',
    Oppervlakte: '11,59',
    AantalInwoners2016: 14.764,
    Deelgemeenten: 'Machelen, Diegem',

  },
  {
    Gemeente: 'Maldegem',
    StadOfGemeente: 'gem.',
    Provincie: 'Oost-Vlaanderen',
    Oppervlakte: '94,64',
    AantalInwoners2016: 23.452,
    Deelgemeenten: 'Maldegem, Adegem, Middelburg',

  },
  {
    Gemeente: 'Malle',
    StadOfGemeente: 'gem.',
    Provincie: 'Antwerpen',
    Oppervlakte: '51,99',
    AantalInwoners2016: 15.158,
    Deelgemeenten: 'Oostmalle, Westmalle',

  },
  {
    Gemeente: 'Mechelen',
    StadOfGemeente: 'stad',
    Provincie: 'Antwerpen',
    Oppervlakte: '65,19',
    AantalInwoners2016: 84.523,
    Deelgemeenten: 'Mechelen, Walem, Heffen, Hombeek, Leest, Muizen',

  },
  {
    Gemeente: 'Meerhout',
    StadOfGemeente: 'gem.',
    Provincie: 'Antwerpen',
    Oppervlakte: '36,29',
    AantalInwoners2016: 10.162,
    Deelgemeenten: 'Meerhout',

  },
  {
    Gemeente: 'Meise',
    StadOfGemeente: 'gem.',
    Provincie: 'Vlaams-Brabant',
    Oppervlakte: '34,82',
    AantalInwoners2016: 18.742,
    Deelgemeenten: 'Meise, Wolvertem',

  },
  {
    Gemeente: 'Melle',
    StadOfGemeente: 'gem.',
    Provincie: 'Oost-Vlaanderen',
    Oppervlakte: '15,21',
    AantalInwoners2016: 11.189,
    Deelgemeenten: 'Melle, Gontrode',

  },
  {
    Gemeente: 'Menen',
    StadOfGemeente: 'stad',
    Provincie: 'West-Vlaanderen',
    Oppervlakte: '33,07',
    AantalInwoners2016: 32.877,
    Deelgemeenten: 'Menen, Lauwe, Rekkem',

  },
  {
    Gemeente: 'Merchtem',
    StadOfGemeente: 'gem.',
    Provincie: 'Vlaams-Brabant',
    Oppervlakte: '36,72',
    AantalInwoners2016: 16.083,
    Deelgemeenten: 'Merchtem, Brussegem, Hamme',

  },
  {
    Gemeente: 'Merelbeke',
    StadOfGemeente: 'gem.',
    Provincie: 'Oost-Vlaanderen',
    Oppervlakte: '36,65',
    AantalInwoners2016: 24.279,
    Deelgemeenten: 'Merelbeke, Bottelare, Lemberge, Melsen, Munte, Schelderode',

  },
  {
    Gemeente: 'Merksplas',
    StadOfGemeente: 'gem.',
    Provincie: 'Antwerpen',
    Oppervlakte: '44,56',
    AantalInwoners2016: 8.642,
    Deelgemeenten: 'Merksplas',

  },
  {
    Gemeente: 'Mesen',
    StadOfGemeente: 'stad[2]',
    Provincie: 'West-Vlaanderen',
    Oppervlakte: '3,58',
    AantalInwoners2016: 1.054,
    Deelgemeenten: 'Mesen',

  },
  {
    Gemeente: 'Meulebeke',
    StadOfGemeente: 'gem.',
    Provincie: 'West-Vlaanderen',
    Oppervlakte: '29,35',
    AantalInwoners2016: 11.018,
    Deelgemeenten: 'Meulebeke',

  },
  {
    Gemeente: 'Middelkerke',
    StadOfGemeente: 'gem.',
    Provincie: 'West-Vlaanderen',
    Oppervlakte: '75,65',
    AantalInwoners2016: 19.262,
    Deelgemeenten: 'Middelkerke, Wilskerke, Leffinge, Mannekensvere, Schore, Sint-Pieters-Kapelle, Slijpe, Lombardsijde, Westende',

  },
  {
    Gemeente: 'Moerbeke',
    StadOfGemeente: 'gem.',
    Provincie: 'Oost-Vlaanderen',
    Oppervlakte: '37,80',
    AantalInwoners2016: 6.328,
    Deelgemeenten: 'Moerbeke',

  },
  {
    Gemeente: 'Mol',
    StadOfGemeente: 'gem.',
    Provincie: 'Antwerpen',
    Oppervlakte: '114,26',
    AantalInwoners2016: 36.034,
    Deelgemeenten: 'Mol',

  },
  {
    Gemeente: 'Moorslede',
    StadOfGemeente: 'gem.',
    Provincie: 'West-Vlaanderen',
    Oppervlakte: '35,34',
    AantalInwoners2016: 10.949,
    Deelgemeenten: 'Moorslede, Dadizele',

  },
  {
    Gemeente: 'Mortsel',
    StadOfGemeente: 'stad',
    Provincie: 'Antwerpen',
    Oppervlakte: '7,78',
    AantalInwoners2016: 25.551,
    Deelgemeenten: 'Mortsel',

  },
  {
    Gemeente: 'Nazareth',
    StadOfGemeente: 'gem.',
    Provincie: 'Oost-Vlaanderen',
    Oppervlakte: '35,19',
    AantalInwoners2016: 11.57,
    Deelgemeenten: 'Nazareth, Eke',

  },
  {
    Gemeente: 'Niel',
    StadOfGemeente: 'gem.',
    Provincie: 'Antwerpen',
    Oppervlakte: '5,27',
    AantalInwoners2016: 10.136,
    Deelgemeenten: 'Niel',

  },
  {
    Gemeente: 'Nieuwerkerken',
    StadOfGemeente: 'gem.',
    Provincie: 'Limburg',
    Oppervlakte: '22,46',
    AantalInwoners2016: 6.865,
    Deelgemeenten: 'Nieuwerkerken, Binderveld, Kozen, Wijer',

  },
  {
    Gemeente: 'Nieuwpoort',
    StadOfGemeente: 'stad',
    Provincie: 'West-Vlaanderen',
    Oppervlakte: '31,00',
    AantalInwoners2016: 11.379,
    Deelgemeenten: 'Nieuwpoort, Ramskapelle, Sint-Joris',

  },
  {
    Gemeente: 'Nijlen',
    StadOfGemeente: 'gem.',
    Provincie: 'Antwerpen',
    Oppervlakte: '39,09',
    AantalInwoners2016: 22.651,
    Deelgemeenten: 'Nijlen, Bevel, Kessel',

  },
  {
    Gemeente: 'Ninove',
    StadOfGemeente: 'stad',
    Provincie: 'Oost-Vlaanderen',
    Oppervlakte: '72,57',
    AantalInwoners2016: 38.051,
    Deelgemeenten: 'Ninove, Appelterre-Eichem, Denderwindeke, Lieferinge, Nederhasselt, Okegem, Voorde, Pollare, Meerbeke, Neigem, Aspelare, Outer',

  },
  {
    Gemeente: 'Olen',
    StadOfGemeente: 'gem.',
    Provincie: 'Antwerpen',
    Oppervlakte: '23,17',
    AantalInwoners2016: 12.242,
    Deelgemeenten: 'Olen',

  },
  {
    Gemeente: 'Oostende',
    StadOfGemeente: 'stad',
    Provincie: 'West-Vlaanderen',
    Oppervlakte: '37,72',
    AantalInwoners2016: 70.6,
    Deelgemeenten: 'Oostende, Stene, Zandvoorde',

  },
  {
    Gemeente: 'Oosterzele',
    StadOfGemeente: 'gem.',
    Provincie: 'Oost-Vlaanderen',
    Oppervlakte: '43,12',
    AantalInwoners2016: 13.545,
    Deelgemeenten: 'Oosterzele, Balegem, Gijzenzele, Landskouter, Moortsele, Scheldewindeke',

  },
  {
    Gemeente: 'Oostkamp',
    StadOfGemeente: 'gem.',
    Provincie: 'West-Vlaanderen',
    Oppervlakte: '79,65',
    AantalInwoners2016: 23.132,
    Deelgemeenten: 'Oostkamp, Hertsberge, Ruddervoorde, Waardamme',

  },
  {
    Gemeente: 'Oostrozebeke',
    StadOfGemeente: 'gem.',
    Provincie: 'West-Vlaanderen',
    Oppervlakte: '16,62',
    AantalInwoners2016: 7.7,
    Deelgemeenten: 'Oostrozebeke',

  },
  {
    Gemeente: 'Opwijk',
    StadOfGemeente: 'gem.',
    Provincie: 'Vlaams-Brabant',
    Oppervlakte: '19,69',
    AantalInwoners2016: 14.208,
    Deelgemeenten: 'Opwijk, Mazenzele',

  },
  {
    Gemeente: 'Oudenaarde',
    StadOfGemeente: 'stad',
    Provincie: 'Oost-Vlaanderen',
    Oppervlakte: '68,06',
    AantalInwoners2016: 30.99,
    Deelgemeenten: 'Oudenaarde, Bevere, Edelare, Eine, Ename, Heurne, Leupegem, Mater, Melden, Mullem, Nederename, Volkegem, Welden',

  },
  {
    Gemeente: 'Oudenburg',
    StadOfGemeente: 'stad',
    Provincie: 'West-Vlaanderen',
    Oppervlakte: '35,38',
    AantalInwoners2016: 9.279,
    Deelgemeenten: 'Oudenburg, Ettelgem, Roksem, Westkerke',

  },
  {
    Gemeente: 'Oud-Heverlee',
    StadOfGemeente: 'gem.',
    Provincie: 'Vlaams-Brabant',
    Oppervlakte: '31,15',
    AantalInwoners2016: 10.973,
    Deelgemeenten: 'Oud-Heverlee, Sint-Joris-Weert, Blanden, Haasrode, Vaalbeek',

  },
  {
    Gemeente: 'Oudsbergen[1]',
    StadOfGemeente: 'gem.',
    Provincie: 'Limburg',
    Oppervlakte: '116,24',
    AantalInwoners2016: 23.379,
    Deelgemeenten: 'Ellikom, Gruitrode, Meeuwen, Neerglabbeek, Opglabbeek, Wijshagen',

  },
  {
    Gemeente: 'Oud-Turnhout',
    StadOfGemeente: 'gem.',
    Provincie: 'Antwerpen',
    Oppervlakte: '38,80',
    AantalInwoners2016: 13.307,
    Deelgemeenten: 'Oud-Turnhout',

  },
  {
    Gemeente: 'Overijse',
    StadOfGemeente: 'gem.',
    Provincie: 'Vlaams-Brabant',
    Oppervlakte: '44,43',
    AantalInwoners2016: 24.959,
    Deelgemeenten: 'Overijse',

  },
  {
    Gemeente: 'Peer',
    StadOfGemeente: 'stad',
    Provincie: 'Limburg',
    Oppervlakte: '86,95',
    AantalInwoners2016: 16.312,
    Deelgemeenten: 'Peer, Grote-Brogel, Kleine-Brogel, Wijchmaal',

  },
  {
    Gemeente: 'Pelt[1]',
    StadOfGemeente: 'gem.',
    Provincie: 'Limburg',
    Oppervlakte: '83,63',
    AantalInwoners2016: 31.92,
    Deelgemeenten: 'Neerpelt, Overpelt, Sint-Huibrechts-Lille',

  },
  {
    Gemeente: 'Pepingen',
    StadOfGemeente: 'gem.',
    Provincie: 'Vlaams-Brabant',
    Oppervlakte: '36,05',
    AantalInwoners2016: 4.424,
    Deelgemeenten: 'Pepingen, Bogaarden, Heikruis, Elingen, Beert, Bellingen',

  },
  {
    Gemeente: 'Pittem',
    StadOfGemeente: 'gem.',
    Provincie: 'West-Vlaanderen',
    Oppervlakte: '34,42',
    AantalInwoners2016: 6.723,
    Deelgemeenten: 'Pittem, Egem',

  },
  {
    Gemeente: 'Poperinge',
    StadOfGemeente: 'stad',
    Provincie: 'West-Vlaanderen',
    Oppervlakte: '119,33',
    AantalInwoners2016: 19.755,
    Deelgemeenten: 'Poperinge, Reningelst, Krombeke, Proven, Roesbrugge-Haringe, Watou',

  },
  {
    Gemeente: 'Putte',
    StadOfGemeente: 'gem.',
    Provincie: 'Antwerpen',
    Oppervlakte: '34,96',
    AantalInwoners2016: 17.227,
    Deelgemeenten: 'Putte, Beerzel',

  },
  {
    Gemeente: 'Puurs-Sint-Amands[1]',
    StadOfGemeente: 'gem.',
    Provincie: 'Antwerpen',
    Oppervlakte: '48,99',
    AantalInwoners2016: 25.331,
    Deelgemeenten: 'Breendonk, Liezele, Lippelo, Oppuurs, Puurs, Ruisbroek, Sint-Amands',

  },
  {
    Gemeente: 'Ranst',
    StadOfGemeente: 'gem.',
    Provincie: 'Antwerpen',
    Oppervlakte: '43,58',
    AantalInwoners2016: 18.856,
    Deelgemeenten: 'Ranst, Broechem, Emblem, Oelegem',

  },
  {
    Gemeente: 'Ravels',
    StadOfGemeente: 'gem.',
    Provincie: 'Antwerpen',
    Oppervlakte: '94,99',
    AantalInwoners2016: 14.735,
    Deelgemeenten: 'Ravels, Weelde, Poppel',

  },
  {
    Gemeente: 'Retie',
    StadOfGemeente: 'gem.',
    Provincie: 'Antwerpen',
    Oppervlakte: '48,39',
    AantalInwoners2016: 11.096,
    Deelgemeenten: 'Retie',

  },
  {
    Gemeente: 'Riemst',
    StadOfGemeente: 'gem.',
    Provincie: 'Limburg',
    Oppervlakte: '57,88',
    AantalInwoners2016: 16.572,
    Deelgemeenten: 'Riemst, Genoelselderen, Herderen, Kanne, Membruggen, Millen, Val-Meer, Vlijtingen, Vroenhoven, Zichen-Zussen-Bolder',

  },
  {
    Gemeente: 'Rijkevorsel',
    StadOfGemeente: 'gem.',
    Provincie: 'Antwerpen',
    Oppervlakte: '46,79',
    AantalInwoners2016: 11.823,
    Deelgemeenten: 'Rijkevorsel',

  },
  {
    Gemeente: 'Roeselare',
    StadOfGemeente: 'stad',
    Provincie: 'West-Vlaanderen',
    Oppervlakte: '59,79',
    AantalInwoners2016: 60.999,
    Deelgemeenten: 'Roeselare, Beveren, Oekene, Rumbeke',

  },
  {
    Gemeente: 'Ronse',
    StadOfGemeente: 'stad[2]',
    Provincie: 'Oost-Vlaanderen',
    Oppervlakte: '34,48',
    AantalInwoners2016: 25.925,
    Deelgemeenten: 'Ronse',

  },
  {
    Gemeente: 'Roosdaal',
    StadOfGemeente: 'gem.',
    Provincie: 'Vlaams-Brabant',
    Oppervlakte: '21,69',
    AantalInwoners2016: 11.494,
    Deelgemeenten: 'Onze-Lieve-Vrouw-Lombeek, Pamel, Strijtem, Borchtlombeek',

  },
  {
    Gemeente: 'Rotselaar',
    StadOfGemeente: 'gem.',
    Provincie: 'Vlaams-Brabant',
    Oppervlakte: '37,57',
    AantalInwoners2016: 16.439,
    Deelgemeenten: 'Rotselaar, Wezemaal, Werchter',

  },
  {
    Gemeente: 'Ruiselede',
    StadOfGemeente: 'gem.',
    Provincie: 'West-Vlaanderen',
    Oppervlakte: '30,20',
    AantalInwoners2016: 5.363,
    Deelgemeenten: 'Ruiselede',

  },
  {
    Gemeente: 'Rumst',
    StadOfGemeente: 'gem.',
    Provincie: 'Antwerpen',
    Oppervlakte: '19,90',
    AantalInwoners2016: 15.14,
    Deelgemeenten: 'Rumst, Reet, Terhagen',

  },
  {
    Gemeente: 'Schelle',
    StadOfGemeente: 'gem.',
    Provincie: 'Antwerpen',
    Oppervlakte: '7,80',
    AantalInwoners2016: 8.259,
    Deelgemeenten: 'Schelle',

  },
  {
    Gemeente: 'Scherpenheuvel-Zichem',
    StadOfGemeente: 'stad',
    Provincie: 'Vlaams-Brabant',
    Oppervlakte: '50,50',
    AantalInwoners2016: 22.833,
    Deelgemeenten: 'Scherpenheuvel, Averbode, Zichem, Messelbroek, Testelt',

  },
  {
    Gemeente: 'Schilde',
    StadOfGemeente: 'gem.',
    Provincie: 'Antwerpen',
    Oppervlakte: '35,99',
    AantalInwoners2016: 19.434,
    Deelgemeenten: 'Schilde, \'s-Gravenwezel',

  },
  {
    Gemeente: 'Schoten',
    StadOfGemeente: 'gem.',
    Provincie: 'Antwerpen',
    Oppervlakte: '29,55',
    AantalInwoners2016: 34.063,
    Deelgemeenten: 'Schoten',

  },
  {
    Gemeente: 'Sint-Genesius-Rode',
    StadOfGemeente: 'gem.[2]',
    Provincie: 'Vlaams-Brabant',
    Oppervlakte: '22,77',
    AantalInwoners2016: 18.171,
    Deelgemeenten: 'Sint-Genesius-Rode',

  },
  {
    Gemeente: 'Sint-Gillis-Waas',
    StadOfGemeente: 'gem.',
    Provincie: 'Oost-Vlaanderen',
    Oppervlakte: '54,98',
    AantalInwoners2016: 19.204,
    Deelgemeenten: 'Sint-Gillis-Waas, De Klinge, Meerdonk, Sint-Pauwels',

  },
  {
    Gemeente: 'Sint-Katelijne-Waver',
    StadOfGemeente: 'gem.',
    Provincie: 'Antwerpen',
    Oppervlakte: '36,12',
    AantalInwoners2016: 20.609,
    Deelgemeenten: 'Sint-Katelijne-Waver, Onze-Lieve-Vrouw-Waver',

  },
  {
    Gemeente: 'Sint-Laureins',
    StadOfGemeente: 'gem.',
    Provincie: 'Oost-Vlaanderen',
    Oppervlakte: '74,50',
    AantalInwoners2016: 6.666,
    Deelgemeenten: 'Sint-Laureins, Sint-Jan-in-Eremo, Sint-Margriete, Waterland-Oudeman, Watervliet',

  },
  {
    Gemeente: 'Sint-Lievens-Houtem',
    StadOfGemeente: 'gem.',
    Provincie: 'Oost-Vlaanderen',
    Oppervlakte: '26,67',
    AantalInwoners2016: 10.152,
    Deelgemeenten: 'Sint-Lievens-Houtem, Letterhoutem, Bavegem, Zonnegem, Vlierzele',

  },
  {
    Gemeente: 'Sint-Martens-Latem',
    StadOfGemeente: 'gem.',
    Provincie: 'Oost-Vlaanderen',
    Oppervlakte: '14,34',
    AantalInwoners2016: 8.517,
    Deelgemeenten: 'Sint-Martens-Latem, Deurle',

  },
  {
    Gemeente: 'Sint-Niklaas',
    StadOfGemeente: 'stad',
    Provincie: 'Oost-Vlaanderen',
    Oppervlakte: '83,80',
    AantalInwoners2016: 75.208,
    Deelgemeenten: 'Sint-Niklaas, Nieuwkerken-Waas, Belsele, Sinaai',

  },
  {
    Gemeente: 'Sint-Pieters-Leeuw',
    StadOfGemeente: 'gem.',
    Provincie: 'Vlaams-Brabant',
    Oppervlakte: '40,38',
    AantalInwoners2016: 33.512,
    Deelgemeenten: 'Sint-Pieters-Leeuw, Oudenaken, Sint-Laureins-Berchem, Ruisbroek, Vlezenbeek',

  },
  {
    Gemeente: 'Sint-Truiden',
    StadOfGemeente: 'stad',
    Provincie: 'Limburg',
    Oppervlakte: '106,90',
    AantalInwoners2016: 40.158,
    Deelgemeenten: 'Sint-Truiden, Aalst, Brustem, Engelmanshoven, Gelinden, Groot-Gelmen, Halmaal, Kerkom-bij-Sint-Truiden, Ordingen, Zepperen, Duras, Gorsem, Runkelen, Wilderen, Velm',

  },
  {
    Gemeente: 'Spiere-Helkijn',
    StadOfGemeente: 'gem.[2]',
    Provincie: 'West-Vlaanderen',
    Oppervlakte: '10,78',
    AantalInwoners2016: 2.133,
    Deelgemeenten: 'Helkijn, Spiere',

  },
  {
    Gemeente: 'Stabroek',
    StadOfGemeente: 'gem.',
    Provincie: 'Antwerpen',
    Oppervlakte: '21,51',
    AantalInwoners2016: 18.515,
    Deelgemeenten: 'Stabroek, Hoevenen',

  },
  {
    Gemeente: 'Staden',
    StadOfGemeente: 'gem.',
    Provincie: 'West-Vlaanderen',
    Oppervlakte: '46,24',
    AantalInwoners2016: 11.173,
    Deelgemeenten: 'Staden, Oostnieuwkerke, Westrozebeke',

  },
  {
    Gemeente: 'Steenokkerzeel',
    StadOfGemeente: 'gem.',
    Provincie: 'Vlaams-Brabant',
    Oppervlakte: '23,46',
    AantalInwoners2016: 11.924,
    Deelgemeenten: 'Steenokkerzeel, Melsbroek, Perk',

  },
  {
    Gemeente: 'Stekene',
    StadOfGemeente: 'gem.',
    Provincie: 'Oost-Vlaanderen',
    Oppervlakte: '44,80',
    AantalInwoners2016: 17.888,
    Deelgemeenten: 'Stekene, Kemzeke',

  },
  {
    Gemeente: 'Temse',
    StadOfGemeente: 'gem.',
    Provincie: 'Oost-Vlaanderen',
    Oppervlakte: '39,92',
    AantalInwoners2016: 29.194,
    Deelgemeenten: 'Temse, Elversele, Steendorp, Tielrode',

  },
  {
    Gemeente: 'Ternat',
    StadOfGemeente: 'gem.',
    Provincie: 'Vlaams-Brabant',
    Oppervlakte: '24,48',
    AantalInwoners2016: 15.286,
    Deelgemeenten: 'Ternat, Sint-Katherina-Lombeek, Wambeek',

  },
  {
    Gemeente: 'Tervuren',
    StadOfGemeente: 'gem.',
    Provincie: 'Vlaams-Brabant',
    Oppervlakte: '32,92',
    AantalInwoners2016: 21.572,
    Deelgemeenten: 'Tervuren, Duisburg, Vossem',

  },
  {
    Gemeente: 'Tessenderlo',
    StadOfGemeente: 'gem.',
    Provincie: 'Limburg',
    Oppervlakte: '51,35',
    AantalInwoners2016: 18.403,
    Deelgemeenten: 'Tessenderlo',

  },
  {
    Gemeente: 'Tielt',
    StadOfGemeente: 'stad',
    Provincie: 'West-Vlaanderen',
    Oppervlakte: '68,50',
    AantalInwoners2016: 20.159,
    Deelgemeenten: 'Tielt, Aarsele, Kanegem, Schuiferskapelle',

  },
  {
    Gemeente: 'Tielt-Winge',
    StadOfGemeente: 'gem.',
    Provincie: 'Vlaams-Brabant',
    Oppervlakte: '44,16',
    AantalInwoners2016: 10.674,
    Deelgemeenten: 'Houwaart, Meensel-Kiezegem, Sint-Joris-Winge, Tielt',

  },
  {
    Gemeente: 'Tienen',
    StadOfGemeente: 'stad',
    Provincie: 'Vlaams-Brabant',
    Oppervlakte: '71,77',
    AantalInwoners2016: 34.185,
    Deelgemeenten: 'Tienen, Bost, Goetsenhoven, Hakendover, Kumtich, Oorbeek, Oplinter, Sint-Margriete-Houtem, Vissenaken',

  },
  {
    Gemeente: 'Tongeren',
    StadOfGemeente: 'stad',
    Provincie: 'Limburg',
    Oppervlakte: '87,56',
    AantalInwoners2016: 30.72,
    Deelgemeenten: 'Tongeren, Berg, Diets-Heur, Henis, \'s Herenelderen, Koninksem, Lauw, Mal, Neerrepen, Nerem, Overrepen, Piringen, Riksingen, Rutten, Sluizen, Vreren, Widooie',

  },
  {
    Gemeente: 'Torhout',
    StadOfGemeente: 'stad',
    Provincie: 'West-Vlaanderen',
    Oppervlakte: '45,23',
    AantalInwoners2016: 20.486,
    Deelgemeenten: 'Torhout',

  },
  {
    Gemeente: 'Tremelo',
    StadOfGemeente: 'gem.',
    Provincie: 'Vlaams-Brabant',
    Oppervlakte: '21,57',
    AantalInwoners2016: 14.733,
    Deelgemeenten: 'Tremelo, Baal',

  },
  {
    Gemeente: 'Turnhout',
    StadOfGemeente: 'stad',
    Provincie: 'Antwerpen',
    Oppervlakte: '56,06',
    AantalInwoners2016: 42.965,
    Deelgemeenten: 'Turnhout',

  },
  {
    Gemeente: 'Veurne',
    StadOfGemeente: 'stad',
    Provincie: 'West-Vlaanderen',
    Oppervlakte: '96,34',
    AantalInwoners2016: 11.674,
    Deelgemeenten: 'Veurne, Avekapelle, Booitshoeke, Bulskamp, De Moeren, Eggewaartskapelle, Houtem, Steenkerke, Vinkem, Wulveringem, Zoutenaaie',

  },
  {
    Gemeente: 'Vilvoorde',
    StadOfGemeente: 'stad',
    Provincie: 'Vlaams-Brabant',
    Oppervlakte: '21,48',
    AantalInwoners2016: 43.017,
    Deelgemeenten: 'Vilvoorde, Peutie',

  },
  {
    Gemeente: 'Vleteren',
    StadOfGemeente: 'gem.',
    Provincie: 'West-Vlaanderen',
    Oppervlakte: '38,15',
    AantalInwoners2016: 3.673,
    Deelgemeenten: 'Oostvleteren, Westvleteren, Woesten',

  },
  {
    Gemeente: 'Voeren',
    StadOfGemeente: 'gem.[2]',
    Provincie: 'Limburg',
    Oppervlakte: '50,63',
    AantalInwoners2016: 4.099,
    Deelgemeenten: 'Moelingen, Remersdaal, \'s-Gravenvoeren, Sint-Martens-Voeren, Sint-Pieters-Voeren, Teuven',

  },
  {
    Gemeente: 'Vorselaar',
    StadOfGemeente: 'gem.',
    Provincie: 'Antwerpen',
    Oppervlakte: '27,62',
    AantalInwoners2016: 7.777,
    Deelgemeenten: 'Vorselaar',

  },
  {
    Gemeente: 'Vosselaar',
    StadOfGemeente: 'gem.',
    Provincie: 'Antwerpen',
    Oppervlakte: '11,85',
    AantalInwoners2016: 10.935,
    Deelgemeenten: 'Vosselaar',

  },
  {
    Gemeente: 'Waasmunster',
    StadOfGemeente: 'gem.',
    Provincie: 'Oost-Vlaanderen',
    Oppervlakte: '31,93',
    AantalInwoners2016: 10.736,
    Deelgemeenten: 'Waasmunster',

  },
  {
    Gemeente: 'Wachtebeke',
    StadOfGemeente: 'gem.',
    Provincie: 'Oost-Vlaanderen',
    Oppervlakte: '34,53',
    AantalInwoners2016: 7.588,
    Deelgemeenten: 'Wachtebeke',

  },
  {
    Gemeente: 'Waregem',
    StadOfGemeente: 'stad',
    Provincie: 'West-Vlaanderen',
    Oppervlakte: '44,34',
    AantalInwoners2016: 37.606,
    Deelgemeenten: 'Waregem, Beveren, Desselgem, Sint-Eloois-Vijve',

  },
  {
    Gemeente: 'Wellen',
    StadOfGemeente: 'gem.',
    Provincie: 'Limburg',
    Oppervlakte: '26,72',
    AantalInwoners2016: 7.374,
    Deelgemeenten: 'Wellen, Berlingen, Herten, Ulbeek',

  },
  {
    Gemeente: 'Wemmel',
    StadOfGemeente: 'gem.[2]',
    Provincie: 'Vlaams-Brabant',
    Oppervlakte: '8,74',
    AantalInwoners2016: 16.059,
    Deelgemeenten: 'Wemmel',

  },
  {
    Gemeente: 'Wervik',
    StadOfGemeente: 'stad',
    Provincie: 'West-Vlaanderen',
    Oppervlakte: '43,61',
    AantalInwoners2016: 18.529,
    Deelgemeenten: 'Wervik, Geluwe',

  },
  {
    Gemeente: 'Westerlo',
    StadOfGemeente: 'gem.',
    Provincie: 'Antwerpen',
    Oppervlakte: '55,13',
    AantalInwoners2016: 24.705,
    Deelgemeenten: 'Westerlo, Oevel, Tongerlo, Zoerle-Parwijs',

  },
  {
    Gemeente: 'Wetteren',
    StadOfGemeente: 'gem.',
    Provincie: 'Oost-Vlaanderen',
    Oppervlakte: '36,68',
    AantalInwoners2016: 24.818,
    Deelgemeenten: 'Wetteren, Massemen, Westrem',

  },
  {
    Gemeente: 'Wevelgem',
    StadOfGemeente: 'gem.',
    Provincie: 'West-Vlaanderen',
    Oppervlakte: '38,76',
    AantalInwoners2016: 31.291,
    Deelgemeenten: 'Wevelgem, Gullegem, Moorsele',

  },
  {
    Gemeente: 'Wezembeek-Oppem',
    StadOfGemeente: 'gem.[2]',
    Provincie: 'Vlaams-Brabant',
    Oppervlakte: '6,82',
    AantalInwoners2016: 14.095,
    Deelgemeenten: 'Wezembeek-Oppem',

  },
  {
    Gemeente: 'Wichelen',
    StadOfGemeente: 'gem.',
    Provincie: 'Oost-Vlaanderen',
    Oppervlakte: '22,87',
    AantalInwoners2016: 11.501,
    Deelgemeenten: 'Wichelen, Schellebelle, Serskamp',

  },
  {
    Gemeente: 'Wielsbeke',
    StadOfGemeente: 'gem.',
    Provincie: 'West-Vlaanderen',
    Oppervlakte: '21,76',
    AantalInwoners2016: 9.525,
    Deelgemeenten: 'Wielsbeke, Ooigem, Sint-Baafs-Vijve',

  },
  {
    Gemeente: 'Wijnegem',
    StadOfGemeente: 'gem.',
    Provincie: 'Antwerpen',
    Oppervlakte: '7,86',
    AantalInwoners2016: 9.365,
    Deelgemeenten: 'Wijnegem',

  },
  {
    Gemeente: 'Willebroek',
    StadOfGemeente: 'gem.',
    Provincie: 'Antwerpen',
    Oppervlakte: '27,41',
    AantalInwoners2016: 25.745,
    Deelgemeenten: 'Willebroek, Blaasveld, Heindonk, Tisselt',

  },
  {
    Gemeente: 'Wingene',
    StadOfGemeente: 'gem.',
    Provincie: 'West-Vlaanderen',
    Oppervlakte: '68,42',
    AantalInwoners2016: 14.209,
    Deelgemeenten: 'Wingene, Zwevezele',

  },
  {
    Gemeente: 'Wommelgem',
    StadOfGemeente: 'gem.',
    Provincie: 'Antwerpen',
    Oppervlakte: '13,01',
    AantalInwoners2016: 12.591,
    Deelgemeenten: 'Wommelgem',

  },
  {
    Gemeente: 'Wortegem-Petegem',
    StadOfGemeente: 'gem.',
    Provincie: 'Oost-Vlaanderen',
    Oppervlakte: '41,96',
    AantalInwoners2016: 6.326,
    Deelgemeenten: 'Elsegem, Moregem, Ooike, Petegem-aan-de-Schelde, Wortegem',

  },
  {
    Gemeente: 'Wuustwezel',
    StadOfGemeente: 'gem.',
    Provincie: 'Antwerpen',
    Oppervlakte: '89,43',
    AantalInwoners2016: 20.228,
    Deelgemeenten: 'Wuustwezel, Loenhout',

  },
  {
    Gemeente: 'Zandhoven',
    StadOfGemeente: 'gem.',
    Provincie: 'Antwerpen',
    Oppervlakte: '40,10',
    AantalInwoners2016: 12.768,
    Deelgemeenten: 'Zandhoven, Massenhoven, Pulderbos, Pulle, Viersel',

  },
  {
    Gemeente: 'Zaventem',
    StadOfGemeente: 'gem.',
    Provincie: 'Vlaams-Brabant',
    Oppervlakte: '27,62',
    AantalInwoners2016: 33.034,
    Deelgemeenten: 'Zaventem, Nossegem, Sint-Stevens-Woluwe, Sterrebeek',

  },
  {
    Gemeente: 'Zedelgem',
    StadOfGemeente: 'gem.',
    Provincie: 'West-Vlaanderen',
    Oppervlakte: '60,34',
    AantalInwoners2016: 22.479,
    Deelgemeenten: 'Aartrijke, Loppem, Veldegem, Zedelgem',

  },
  {
    Gemeente: 'Zele',
    StadOfGemeente: 'gem.',
    Provincie: 'Oost-Vlaanderen',
    Oppervlakte: '33,06',
    AantalInwoners2016: 20.785,
    Deelgemeenten: 'Zele',

  },
  {
    Gemeente: 'Zelzate',
    StadOfGemeente: 'gem.',
    Provincie: 'Oost-Vlaanderen',
    Oppervlakte: '13,71',
    AantalInwoners2016: 12.49,
    Deelgemeenten: 'Zelzate',

  },
  {
    Gemeente: 'Zemst',
    StadOfGemeente: 'gem.',
    Provincie: 'Vlaams-Brabant',
    Oppervlakte: '42,85',
    AantalInwoners2016: 23.068,
    Deelgemeenten: 'Zemst, Eppegem, Elewijt, Hofstade, Weerde',

  },
  {
    Gemeente: 'Zoersel',
    StadOfGemeente: 'gem.',
    Provincie: 'Antwerpen',
    Oppervlakte: '38,65',
    AantalInwoners2016: 21.764,
    Deelgemeenten: 'Zoersel, Halle',

  },
  {
    Gemeente: 'Zonhoven',
    StadOfGemeente: 'gem.',
    Provincie: 'Limburg',
    Oppervlakte: '39,34',
    AantalInwoners2016: 21.276,
    Deelgemeenten: 'Zonhoven',

  },
  {
    Gemeente: 'Zonnebeke',
    StadOfGemeente: 'gem.',
    Provincie: 'West-Vlaanderen',
    Oppervlakte: '67,57',
    AantalInwoners2016: 12.417,
    Deelgemeenten: 'Zonnebeke, Beselare, Geluveld, Passendale, Zandvoorde',

  },
  {
    Gemeente: 'Zottegem',
    StadOfGemeente: 'stad',
    Provincie: 'Oost-Vlaanderen',
    Oppervlakte: '56,66',
    AantalInwoners2016: 25.899,
    Deelgemeenten: 'Zottegem, Elene, Erwetegem, Godveerdegem, Grotenberge, Leeuwergem, Sint-Goriks-Oudenhove, Strijpen, Velzeke-Ruddershove, Oombergen, Sint-Maria-Oudenhove',

  },
  {
    Gemeente: 'Zoutleeuw',
    StadOfGemeente: 'stad',
    Provincie: 'Vlaams-Brabant',
    Oppervlakte: '46,73',
    AantalInwoners2016: 8.364,
    Deelgemeenten: 'Zoutleeuw, Budingen, Dormaal, Halle-Booienhoven, Helen-Bos',

  },
  {
    Gemeente: 'Zuienkerke',
    StadOfGemeente: 'gem.',
    Provincie: 'West-Vlaanderen',
    Oppervlakte: '48,86',
    AantalInwoners2016: 2.711,
    Deelgemeenten: 'Zuienkerke, Meetkerke, Houtave, Nieuwmunster',

  },
  {
    Gemeente: 'Zulte',
    StadOfGemeente: 'gem.',
    Provincie: 'Oost-Vlaanderen',
    Oppervlakte: '32,52',
    AantalInwoners2016: 15.589,
    Deelgemeenten: 'Zulte, Olsene, Machelen',

  },
  {
    Gemeente: 'Zutendaal',
    StadOfGemeente: 'gem.',
    Provincie: 'Limburg',
    Oppervlakte: '32,07',
    AantalInwoners2016: 7.239,
    Deelgemeenten: 'Zutendaal',

  },
  {
    Gemeente: 'Zwalm',
    StadOfGemeente: 'gem.',
    Provincie: 'Oost-Vlaanderen',
    Oppervlakte: '33,82',
    AantalInwoners2016: 8.04,
    Deelgemeenten: 'Beerlegem, Dikkele, Hundelgem, Meilegem, Munkzwalm, Nederzwalm-Hermelgem, Paulatem, Roborst, Rozebeke, Sint-Blasius-Boekel, Sint-Denijs-Boekel, Sint-Maria-Latem',

  },
  {
    Gemeente: 'Zwevegem',
    StadOfGemeente: 'gem.',
    Provincie: 'West-Vlaanderen',
    Oppervlakte: '63,24',
    AantalInwoners2016: 24.353,
    Deelgemeenten: 'Zwevegem, Heestert, Moen, Otegem, Sint-Denijs',

  },
  {
    Gemeente: 'Zwijndrecht',
    StadOfGemeente: 'gem',
    Provincie: 'Antwerpen',
    Oppervlakte: '17,82',
    AantalInwoners2016: 18.957,
    Deelgemeenten: 'Zwijndrecht, Burcht',

  },
]

export default Plaatsen
