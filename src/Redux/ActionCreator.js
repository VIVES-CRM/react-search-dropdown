import { Actions } from '../Cst'
import Plaatsen from '../Api/Plaatsen'

const BepaalProvincies = () => {
  const AlleProv = Plaatsen.map((plaats) => plaats.Provincie)
  const UniekeProv = new Set(AlleProv)
  const SorteerdeProv = Array.from(UniekeProv).sort()
  return ['', ...SorteerdeProv]
}

const BepaalDeelgemeentes = (deelgemeenten) => {
  const Deelgemeentes = deelgemeenten.split(',')
  return Deelgemeentes.map((deel) => deel.trim())
}

const DeelgemeentesAlsOpties = (Deelgemeentes, Hoofdgemeente) => Deelgemeentes.map((deel) => ({
  name: deel,
  value: `${Hoofdgemeente}-${deel}`,
  Hoofdgemeente: Hoofdgemeente.Gemeente,
  Provincie: Hoofdgemeente.Provincie,
}))

export const KiesPlaats = (GekozenPlaats) => ({
  type: Actions.GekozenPlaats,
  GekozenPlaats,
})


export const OphalenPlaatsen = () => {
  // maak array met alle plaatsen (deelgemeenten) en hun hoofdgemeente
  // const AllePlaatsen = BepaalPlaatsen()

  // maak opties voor search select component
  const PlaatsenAlsOpties = Plaatsen.map((plaats) => {
    const Deelgemeentes = BepaalDeelgemeentes(plaats.Deelgemeenten)
    return ({
      type: 'group',
      name: plaats.Gemeente,
      value: plaats.Gemeente,
      Provincie: plaats.Provincie,
      items: DeelgemeentesAlsOpties(Deelgemeentes, plaats),
    })
  })

  // filter de provincies
  const SorteerdeProv = BepaalProvincies()
  const ProvinciesAlsOpties = SorteerdeProv.map((prov) => ({
    name: prov,
    value: prov,
  }))

  return ({
    type: Actions.OpgehaaldePlaatsen,
    Plaatsen: PlaatsenAlsOpties,
    Provincies: ProvinciesAlsOpties,
  })
}

export const KiesProvincie = (GekozenProvincie) => ({
  type: Actions.KiesProvincie,
  GekozenProvincie: GekozenProvincie.value,
})
