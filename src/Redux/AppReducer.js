import { Actions } from '../Cst'

export const InitialState = {
  Fout: false,
  Foutmelding: null,

  Provincies: [],
  Plaatsen: [], // [ {Gemeente, Plaatst, Provincie ]

  GekozenPlaats: null,
  GekozenProvincie: null,
}


export const AppReducer = (state = InitialState, actie) => {
  switch (actie.type) {
    case Actions.KiesProvincie:
      return {
        ...state,
        GekozenProvincie: actie.GekozenProvincie,
        GekozenPlaats: null,
      }

    case Actions.GekozenPlaats:
      return {
        ...state,
        GekozenPlaats: actie.GekozenPlaats,
      }

    case Actions.OpgehaaldePlaatsen:
      return {
        ...state,
        Plaatsen: actie.Plaatsen,
        Provincies: actie.Provincies,
      }

    default:
      return state
  }
}
