/* eslint {max-len:off} */
export const CstFoutAPIOnbereikbaar = 'Network Error'
export const CstApi = {
  ProdUrlBase: 'https://XXXXXXXXX.azurewebsites.net/', // Azure Functions
  DevUrlBase: 'http://localhost:7071/', // Azure Functions, lokaal debug

  UrlAPIBase: 'api/', // Azure Functions

  Fout: 'Fout bij api',
}

export const CstRoutes = {
  basename: '', // basename moet een leading slash hebben
  siteName: '/',
}

export const CstTekst = {
  Foutmeldingen: {
    ApiOnbereikbaar: 'Er kunnen geen gegevens opgehaald worden. Controleer de internetverbinding.',
  },
  LandingScherm: {
    Titel: 'Welkom',
    Uitleg: 'Hello World',
  },
  NietGevonden: {
    Tekst: 'De pagina is niet gevonden',
  },
  OnbekendeFout: 'Onbekende fout',
}

export const Actions = {
  GekozenPlaats: 'PLAATS',
  OpgehaaldePlaatsen: 'OPGEHAALDE_PLAATSEN',
  KiesProvincie: 'KIES_PROVINCIE',
  BewarenBezig: 'API_BEWAREN_BEZIG',

  Herstarten: 'HERSTARTEN',
  Fout: 'API_FOUT',
}
