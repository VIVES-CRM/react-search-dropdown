import React, { useEffect } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import SelectSearch from 'react-select-search'
import { CstTekst } from '../Cst'

import Dropdown from '../Components/Dropdown'
import { OphalenPlaatsen, KiesPlaats, KiesProvincie } from '../Redux/ActionCreator'


const { LandingScherm: LandingTxt } = CstTekst

const GefilterdePlaatsen = (Plaatsen, GekozenProvincie) => {
  const GefilterdOpProvincie = Plaatsen.filter((plaats) => (
    // geen gekozen provincie -> alle plaatsen
    !GekozenProvincie ? plaats : plaats.Provincie === GekozenProvincie
  ))
  return GefilterdOpProvincie
  // const EnkelPlaatsnamen = GefilterdOpProvincie.map((plaats) => plaats.Plaats)
  // // voeg lege plaats toe om als eerste te tonen in de dropdown -> keuze te maken
  // return ['', ...EnkelPlaatsnamen]
}

const LandingScherm = () => {
  const dispatch = useDispatch()
  const {
    Plaatsen, Provincies, GekozenPlaats, GekozenProvincie,
  } = useSelector((state) => state)


  useEffect(() => {
    dispatch(OphalenPlaatsen())
  }, [dispatch])

  return (
    <React.Fragment>
      <header>
        <h1>{LandingTxt.Titel}</h1>
        <hr />
      </header>
      <main>
        {Provincies && (
          <div>
            <i>(optioneel) Kies de provincie</i>
            &nbsp;
            <SelectSearch
              name="Provincies"
              options={Provincies}
              value={GekozenProvincie || ''}
              onChange={(gekozenProv) => dispatch(KiesProvincie(gekozenProv))}
              search
            />
          </div>
        )}
        <br />
        <br />
        {Plaatsen && (
          <div>
            <b>Kies plaats</b>
            <SelectSearch
              name="Plaatsen"
              value={GekozenPlaats ? GekozenPlaats.value : ''}
              options={GefilterdePlaatsen(Plaatsen, GekozenProvincie)}
              onChange={(gekozen) => dispatch(KiesPlaats(gekozen))}
              search
              autoComplete="on"
            />
            { // toon provincie
              (GekozenPlaats && <p>{`in ${GekozenPlaats.Provincie}`}</p>)
            }
            { // toon eventueel hoofdgemeente
              (GekozenPlaats && GekozenPlaats.name !== GekozenPlaats.groupName)
              && <p>{`met als hoofdgemeente: ${GekozenPlaats.Hoofdgemeente}`}</p>
            }
          </div>
        )}
      </main>
    </React.Fragment>
  )
}

export default LandingScherm
