import React from 'react'
import { BrowserRouter } from 'react-router-dom'

import AppRouter from './AppRouter'
import { CstRoutes } from '../Cst'

const Navigate = () => (
  <BrowserRouter basename={CstRoutes.basename}>
    <AppRouter />
  </BrowserRouter>
)

export default Navigate
