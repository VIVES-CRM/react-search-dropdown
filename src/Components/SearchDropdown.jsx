import React from 'react'
import PropTypes from 'prop-types'
import SelectSearch from 'react-select-search'

const SearchDropdown = ({ Name, Data, Cb }) => (
  <SelectSearch
    name={Name}
    options={Data}
    onChange={(selected) => Cb(selected)}
  />
)

SearchDropdown.propTypes = {
  Name: PropTypes.string.isRequired,
  Data: PropTypes.array.isRequired,
  Cb: PropTypes.func.isRequired,
}

export default SearchDropdown
