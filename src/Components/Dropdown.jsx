import React from 'react'
import PropTypes from 'prop-types'

const Dropdown = ({ Data, Cb }) => (
  <select
    onChange={(event) => Cb(event.target.value)}
  >
    {Data.map((optie) => (
      <option value={optie} key={optie}>{optie}</option>
    ))}

  </select>

)

Dropdown.propTypes = {
  Data: PropTypes.array.isRequired,
  Cb: PropTypes.func.isRequired,
}

export default Dropdown
