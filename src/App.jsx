import React from 'react'
import './App.css'

import Navigate from './Navigation/Navigate'
import Store from './Redux/Store'


/*  TODO: api key als environment var meegeven in REACT_APP_FUNCTIONKEY

*/
const App = () => (
  <Store>

    <Navigate />

  </Store>
)

export default App
